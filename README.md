lapurdDS
========

------


- [Introduction](#introduction)
    - [Changelog](Changelog.md)
- [Contributing](#contributing)
- [Issues](#issues)


----


# Introduction

A Drupal Distribution designed to be used as a basis for building a Dating Service website.

We came up withe the name of the project by backward naming Drupal.
  - _DRUPAL_=>_**LAPURD**_ 
  
and shortening Dating Service.
  - _Dating Service_=>_**DS**_ 
  
  thus the name lapurdDS.



# Contributing

If you find this Project useful here's how you can help:

- Send a Pull Request with your awesome new features and bug fixes
- Help new users with [Issues](https://gitlab.com/VoyaTrax/lapurdDS/issues) they may encounter
- Support the development of this Drupal Distribution with a [donation](http://www.voyatrax.com/donate/)



# Issues

**lapurdDS** is a relatively new project and is active being developed and tested by a thriving community of developers and testers and every release of lapurdDS features many enhancements and bugfixes.

Given the nature of the development and release cycle it is very important that you have the latest version of **lapurdDS** installed because any issue that you encounter might have already been fixed with a newer lapurdDS release.

Install the most recent version of **lapurdDS** for your platform using the [official **lapurdDS** releases](http://www.voyatrax.com/lapurdDS/docs/installation/), which can also be installed using:

